<?php


$db = new PDO('mysql:host=localhost;dbname=crud67;charset=utf8mb4', 'root', '');

$query = "SELECT * FROM `courses` where id = ".$_GET['single'];
$stmt = $db->query($query);
$results = $stmt->fetch(PDO::FETCH_ASSOC);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Course form</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>


<h2>Update Course</h2>

<form action="course-store.php" method="POST" enctype="multipart/form-data">
    <div class="box">
        <hr>
        <div class="form-group">
            <label for="courses_name">Course Name:</label>
            <input type="text" name="courses_name" value="<?php echo $results['courses_name']?>" id="courses_name" class="form-control">
            <input type="hidden" name="id" value="<?php echo $results['id']?>" id="courses_name" class="form-control">
        </div>

        <div class="form-group">
            <label for="courses_code">Course Code:</label>
            <input id="courses_code" type="text" name="courses_code" value="<?php echo $results['courses_code']?>" class="form-control">
        </div>

        <div class="form-group">
            <input type="submit" name="submit" value="Submit" class="btn btn-success">
        </div>
</form>

</body>
</html>
