<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Course form</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>


<h2>Add Course</h2>

<form action="course-store.php" method="POST" enctype="multipart/form-data">
    <div class="box">
        <hr>
        <div class="form-group">
            <label for="courses_name">Course Name:</label>
            <input type="text" name="courses_name" id="courses_name" class="form-control">

        </div>
        <br>
        <div class="form-group">
            <label for="courses_code">Course Code:</label>
            <input id="courses_code" type="text" name="courses_code" class="form-control">
        </div>

        <div class="form-group">
            <input type="submit" name="submit" value="Submit" class="btn btn-success">
        </div>
</form>

</body>
</html>