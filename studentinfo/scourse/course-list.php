<?php

$db = new PDO('mysql:host=localhost;dbname=crud67;charset=utf8mb4', 'root', '');

$query = "SELECT * FROM `courses`";
$stmt = $db->query($query);
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Course form</title>

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/bootstrap-theme.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>

<body>
<div class="container">
    <table class="table table-striped table-bordered" >
        <thead >
        <tr>
            <td>SL</td>
            <td>Course Name</td>
            <td>Course Code</td>
            <td>Action</td>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl = 1;
        foreach ($results as $user){
            ?>
            <tr align="center">
                <td><?php echo $sl++?></td>
                <td><?php echo $user['courses_name'] ?></td>
                <td><?php echo $user['courses_code'] ?></td>

                <td>
                    <a href="course-view.php?single=<?php echo $user['id'] ?>">View</a>
                    <a class="text-danger" href="course-delete.php?single=<?php echo $user['id'] ?>">Delete</a>
                    <a href="course-edit.php?single=<?php echo $user['id'] ?>">Edit</a>
                </td>
            </tr>

        <?php }?>

        </tbody>
    </table>
</div>

</body>
</html>
