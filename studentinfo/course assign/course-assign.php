<?php
$db = new PDO('mysql:host=localhost;dbname=crud67;charset=utf8mb4', 'root', '');
$query = "SELECT id, name FROM students";
$stmt = $db->query($query);
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);


$query2 = "SELECT id, courses_name FROM courses";
$stmt2 = $db->query($query2);
$results2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Course form</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
<div class="container">
    <div align="center"><h2>Assign Course to Student</h2></div>

    <br>

    <form action="course-assign-store.php" method="POST">
        <div class="box">

            <div class="form-group">

                <label>Select Student : </label>


                <select name="id" class="btn btn-default" required>

                    <option value="">Select Student Name</option>

                    <?php

                    foreach ($results as $data) {

                        ?>

                        <option value="<?php echo $data['id']; ?>">

                            <?php echo $data['id']." . ".$data['name']; ?>

                        </option>

                    <?php } //end of first foreach loop ?>
                </select>

            </div>
            <br>
            <div class="form-group">


                <?php

                foreach ($results2 as $data2) {
                    ?>

                    <input id="<?php echo $data2['id']; ?>" type="checkbox" name="id[]" value="<?php echo $data2['id']; ?>">

                    <label for="<?php echo $data2['id']; ?>">
                        <?php echo $data2['courses_name']."  ..  "/* dot dot for space :( */ ; ?></label>


                <?php } //end of second foreach loop ?>

            </div>

            <div class="form-group">
                <input type="submit" name="assign" value="Assign" class="btn btn-success">
            </div>
        </div>
    </form>
</div>


</body>

