<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Student form</title>
    <style>
        .center{

            margin: 0 auto;
            width: 60%;
        }
    </style>

   <!--<link rel="stylesheet" href="../../css/bootstrap.min.css">
	<link rel="stylesheet" href="../../css/bootstrap-theme.css">
	<link rel="stylesheet" href="../../css/style.css">-->
</head>

<body>


	<h2 style="text-align: center;">Add Student</h2>

	<form action="student-store.php" method="POST" enctype="multipart/form-data">
        <div class="center">
            <table border="1">
                <tr>
                    <td>Enter your Name </td>
                    <td ><input type="text" name="name" /></td>
                </tr>
                <tr>
                    <td>Enter your Email </td>
                    <td><input type="email" name="email" /></td>
                </tr>
                <tr>
                    <td>Enter your Password </td>
                    <td><input type="password" name="password" /></td>
                </tr>
                <tr>
                    <td>Enter your Address </td>
                    <td><input type="text" name="address" /></td>
                </tr>
                <tr>
                    <td>Enter your Mobile </td>
                    <td><input type="number" name="mobile" /></td>
                </tr>
                <tr>
                    <td>Select your Gender </td>
                    <td>
                        Male <input type="radio" name="gender" value="male" />
                        Female <input type="radio" name="gender" value="female" />
                    </td>
                </tr>
                <tr>
                    <td>Choose your Hobbies </td>
                    <td>
                        Cricket <input type="checkbox" name="hobby[]" value="cricket" />
                        Singing <input type="checkbox" name="hobby[]" value="singing" />
                        Dancing <input type="checkbox" name="hobby[]" value="dancing" />
                    </td>
                </tr>
                <tr>
                    <td>Choose your Profile Pic </td>
                    <td><input type="file" name="image" /></td>
                </tr>
                <tr>
                    <td>Select your DOB</td>
                    <td>
                        <select name="month" id="month">
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                        <select name="date" id="date">
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                        </select>
                        <select name="year" id="year">
                            <option value="1988">1988</option>
                            <option value="1989">1989</option>
                            <option value="1990">1990</option>
                            <option value="1991">1991</option>
                            <option value="1992">1992</option>
                            <option value="1993">1993</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button type="submit">Register Me</button>
                        <button type="reset">Reset</button>
                    </td>
                </tr>
            </table>
        </div>

    </form>
</body>
</html>