<?php

$db = new PDO('mysql:host=localhost;dbname=crud67;charset=utf8mb4', 'root', '');

$query = "SELECT * FROM `students`";
$stmt = $db->query($query);
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Registration form</title>

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/bootstrap-theme.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>

<body>
<div class="container">
    <table class="table table-striped table-bordered" >
        <thead >
            <tr align="center">
                <td>SL</td>
                <td>Name</td>
                <td>Email</td>
                <!--<td>Password</td>-->
                <td>Address</td>
                <td>Mobile</td>
                <td>Gender</td>
                <td>Hobby</td>
                <!--<td>Image</td>-->
                <td>Date of Birth</td>
                <td>Action</td>
            </tr>
        </thead>
        <tbody>
        <?php
        $sl = 1;
        foreach ($results as $user){
        ?>
            <tr align="center">
                <td><?php echo $sl++?></td>
                <td><?php echo $user['name'] ?></td>
                <td><?php echo $user['email'] ?></td>
               <!-- <td><?php /*echo $user['password'] */?></td>-->
                <td><?php echo $user['address'] ?></td>
                <td><?php echo $user['mobile'] ?></td>
                <td><?php echo $user['gender'] ?></td>
                <td><?php echo $user['hobby'] ?></td>
                <!--<td><?php /*echo $user['image_name']*/?></td>-->
                <td><?php echo $user['dob'] ?></td>

                <td>
                    <a href="student-view.php?single=<?php echo $user['id'] ?>">View</a>
                    <a class="text-danger" href="student-delete.php?single=<?php echo $user['id'] ?>">Delete</a>
                    <a href="student-edit.php?single=<?php echo $user['id'] ?>">Edit</a>
                </td>
            </tr>

        <?php }?>

        </tbody>
    </table>
</div>

</body>
</html>
